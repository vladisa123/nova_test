FROM python:3.9-slim

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
COPY ./requirements.txt .

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY ./src /

WORKDIR /

COPY ./entrypoint.sh /

EXPOSE 8000

ENTRYPOINT ["sh", "/entrypoint.sh"]