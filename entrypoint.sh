#!/bin/sh

python manage.py makemigrations --noinput
python manage.py migrate --noinput
python manage.py collectstatic --noinput

DJANGO_SUPERUSER_PASSWORD=root python manage.py createsuperuser --username root --email root@mail.ru --noinput

gunicorn config.wsgi:application --bind 0.0.0.0:8000
