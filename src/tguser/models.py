from django.db import models


class TgUser(models.Model):
    """ TG user model """

    user_id = models.IntegerField()
