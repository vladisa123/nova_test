from tguser.models import TgUser


def new_tg_user(user_id: int) -> bool:
    """ Check for new tg user in db"""

    _, created = TgUser.objects.get_or_create(user_id=user_id)
    return created
