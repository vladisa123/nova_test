import requests
from telegram import Message


def send_new_contact(message: Message) -> None:
    """ Send new received contact to host """

    headers = {'Content-Type': 'application/json'}

    url = 'https://s1-nova.ru/app/private_test_python/'

    json = {
        'phone': message.contact.phone_number,
        'login': message.from_user.username
    }

    requests.post(url=url, json=json, headers=headers)
