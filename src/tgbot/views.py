import json

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from tgbot.dispatcher import process_telegram_event


@method_decorator(csrf_exempt, name='dispatch')
class TgBotEndpoint(View):
    """ Endpoint: receive new updates from TG """

    def post(self, request):
        process_telegram_event(json.loads(request.body))
        return JsonResponse({"ok": "POST request processed"})
