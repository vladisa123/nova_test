from telegram import KeyboardButton, ReplyKeyboardMarkup

from tgbot.messages import phone_number_request_button_text


def start_keyboard() -> ReplyKeyboardMarkup:
    """ Start|Contact keyboard """

    buttons = [
        [
            KeyboardButton(phone_number_request_button_text, request_contact=True)
        ]
    ]

    return ReplyKeyboardMarkup(keyboard=buttons, resize_keyboard=True)
