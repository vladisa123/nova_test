from telegram import Update
from telegram.ext.utils.types import CCT

from tgbot.keyboards import start_keyboard
from tgbot.messages import start_mess, greats_mess
from tgbot.utils import send_new_contact
from tguser.services import new_tg_user


def start_message(update: Update, context: CCT) -> None:
    """ /start - command handler. Ask contact if new tg user """

    if new_tg_user(user_id=update.message.from_user.id):
        context.bot.send_message(chat_id=update.message.chat.id,
                                 text=start_mess,
                                 reply_markup=start_keyboard())


def receive_contact(update: Update, context: CCT) -> None:
    """ 'Contact' content_type handler """

    send_new_contact(update.message)
    context.bot.send_message(chat_id=update.message.chat.id,
                             text=greats_mess)
