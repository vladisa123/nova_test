from django.urls import path

from .views import *

urlpatterns = [
    path('endpoint/', TgBotEndpoint.as_view()),
]