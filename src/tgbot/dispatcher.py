import telegram
from telegram.ext import CommandHandler, MessageHandler, Filters, Dispatcher
from telegram.utils.types import JSONDict

from config.settings import bot
from tgbot.handlers import start_message, receive_contact


def setup_dispatcher(dp: Dispatcher) -> Dispatcher:
    """ Dispatcher routes configuration """

    dp.add_handler(CommandHandler("start", start_message))
    dp.add_handler(MessageHandler(Filters.contact, receive_contact))

    return dp


def process_telegram_event(update_json: JSONDict) -> None:
    """ Checkout and process of new updates """

    update = telegram.Update.de_json(update_json, bot)
    dispatcher.process_update(update)


dispatcher = setup_dispatcher(Dispatcher(bot=bot, update_queue=None, workers=0, use_context=True))
